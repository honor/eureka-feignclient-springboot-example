package com.eureka.feignclient;

import com.eureka.feignclient.clients.GreetingClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;


@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class FeignclientApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(FeignclientApplication.class, args);
	}

	private GreetingClient greetingClient;

	@Autowired
	public void setGreetingClient(GreetingClient greetingClient) {
		this.greetingClient = greetingClient;
	}

	private Logger LOG = LoggerFactory.getLogger(FeignclientApplication.class);

	public void controllerGreeting(){
		String greeting = greetingClient.greeting();
		LOG.info(greeting);
	}

	@Override
	public void run(String... strings) throws Exception {
		controllerGreeting();
	}
}
