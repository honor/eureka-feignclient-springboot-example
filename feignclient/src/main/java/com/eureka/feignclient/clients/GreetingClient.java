package com.eureka.feignclient.clients;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("eurekaclientapplication")
public interface GreetingClient{

    @RequestMapping("/greeting")
    public String greeting();

}
